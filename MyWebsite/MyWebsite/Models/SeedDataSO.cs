﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using MyWebsite.Data;
using System;
using System.Linq;

namespace MyWebsite.Models
{
    public static class SeedDataSO
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MyWebsiteContext(
               serviceProvider.GetRequiredService<
                   DbContextOptions<MyWebsiteContext>>()))
            {
                if (context.SpaceObject.Any())
                {
                    return;
                }
                context.SpaceObject.AddRange(
                    new SpaceObject
                    {
                        Messier = "107",
                        Common = null,
                        Con = "Oph",
                        Type = "gc",
                        Mag = 8.9F,
                        RA = "163232",
                        DEC = "-130314"

                    },
                     new SpaceObject
                     {
                         Messier = "108",
                         Common = null,
                         Con = "UMa",
                         Type = "gal",
                         Mag = 10.7F,
                         RA = "111131",
                         DEC = "554027"

                     },
                      new SpaceObject
                      {
                          Messier = "109",
                          Common = null,
                          Con = "Uma",
                          Type = "gal",
                          Mag = 10.6F,
                          RA = "115736",
                          DEC = "532228"

                      },
                       new SpaceObject
                       {
                           Messier = "M110",
                           Common = null,
                           Con = "And",
                           Type = "gal",
                           Mag = 9.0F,
                           RA = "004022",
                           DEC = "414107"

                       }
                    );
                context.SaveChanges();
            }
        }
    }
}
