﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MyWebsite.Models
{
    public partial class User
    {
        public int UserID { get; set; }
        public string FullName { get; set; }
        //[Required(ErrorMessage ="Please Provide Username", AllowEmptyStrings = false)]
        public string UserName { get; set; }
        //[Required(ErrorMessage = "Please Provide password", AllowEmptyStrings = false)]
        //[DataType( System.ComponentModel.DataAnnotations.DataType.Password)]
        public string Password { get; set; }
        
    }
}
