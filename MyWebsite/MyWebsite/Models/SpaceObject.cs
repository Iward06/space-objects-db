﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyWebsite.Models
{
    public class SpaceObject
    {
        public int Id { get; set; }
        [Display(Name = "Messier Value")]
        public string Messier { get; set; }
        [Display(Name = "Common Name")]
        public string Common { get; set; }
        [Display(Name = "Constellations")]
        public string Con { get; set; }
        [Display(Name = "Object Type")]
        public string Type { get; set; }
        [Display(Name = "Magnitude")]
        public double Mag { get; set; }
        [Display(Name = "Right Ascension")]
        //[DisplayColumn use this to add leading zeros?
        public string RA { get; set; }
        [Display(Name = "Declination")]
        public string DEC { get; set; }
    }
}