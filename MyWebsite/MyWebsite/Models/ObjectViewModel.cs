﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MyWebsite.Models
{
    public class ObjectViewModel
    {
        public List<SpaceObject> SpaceObjects { get; set; }
        public SelectList  Con { get; set; }
        public SelectList Type { get; set; }
        public string SpaceObjectCon { get; set; }
        public string SpaceObjectType { get; set; }
        public string SearchString { get; set; }
    }
}
