﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyWebsite.Migrations
{
    public partial class Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "Mag",
                table: "SpaceObject",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "SpaceObject",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "SpaceObject");

            migrationBuilder.AlterColumn<float>(
                name: "Mag",
                table: "SpaceObject",
                type: "real",
                nullable: false,
                oldClrType: typeof(float));
        }
    }
}
