﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyWebsite.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SpaceObject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Messier = table.Column<string>(nullable: false),
                    Common = table.Column<string>(nullable: true),
                    Con = table.Column<string>(nullable: false),
                    Mag = table.Column<float>(nullable: false),
                    RA = table.Column<string>(nullable: false),
                    DEC = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpaceObject", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SpaceObject");
        }
    }
}
