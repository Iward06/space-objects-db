﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyWebsite.Data;
using MyWebsite.Models;

namespace MyWebsite.Controllers
{
    public class SpaceObjectsController : Controller
    {
        private readonly MyWebsiteContext _context;

        public SpaceObjectsController(MyWebsiteContext context)
        {
            _context = context;
        }

        // GET: SpaceObjects
        public async Task<IActionResult> Index(string searchString, string spaceobjectCon, string spaceobjectType)
        {
           IQueryable<string> conQuery = from m in _context.SpaceObject
                                          orderby m.Con
                                          select m.Con;
           IQueryable<string> typeQuery = from m in _context.SpaceObject
                                           orderby m.Type
                                           select m.Type;

            var spaceobjects = from m in _context.SpaceObject
                               select m;
            if (!string.IsNullOrEmpty(searchString))
            {
                spaceobjects = spaceobjects.Where(s => s.Messier.Contains(searchString)); 
            }
            if (!string.IsNullOrEmpty(spaceobjectType))
            {
                spaceobjects = spaceobjects.Where(y => y.Type == spaceobjectType);
            }
            if (!string.IsNullOrEmpty(spaceobjectCon))
            {
                spaceobjects = spaceobjects.Where(x => x.Con == spaceobjectCon);
            }
            var spaceobjectVm = new ObjectViewModel
            {
                Con = new SelectList(await conQuery.Distinct().ToListAsync()),
                Type = new SelectList(await typeQuery.Distinct().ToListAsync()),
                SpaceObjects = await spaceobjects.ToListAsync()
            };
            return View(spaceobjectVm);
        }


        // GET: SpaceObjects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaceObject = await _context.SpaceObject
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spaceObject == null)
            {
                return NotFound();
            }

            return View(spaceObject);
        }

        // GET: SpaceObjects/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SpaceObjects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Messier,Common,Con,Type,Mag,RA,DEC")] SpaceObject spaceObject)
        {
            if (ModelState.IsValid)
            {
                _context.Add(spaceObject);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(spaceObject);
        }

        // GET: SpaceObjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaceObject = await _context.SpaceObject.FindAsync(id);
            if (spaceObject == null)
            {
                return NotFound();
            }
            return View(spaceObject);
        }

        // POST: SpaceObjects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Messier,Common,Con,Type,Mag,RA,DEC")] SpaceObject spaceObject)
        {
            if (id != spaceObject.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(spaceObject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpaceObjectExists(spaceObject.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(spaceObject);
        }

        // GET: SpaceObjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaceObject = await _context.SpaceObject
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spaceObject == null)
            {
                return NotFound();
            }

            return View(spaceObject);
        }

        // POST: SpaceObjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var spaceObject = await _context.SpaceObject.FindAsync(id);
            _context.SpaceObject.Remove(spaceObject);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SpaceObjectExists(int id)
        {
            return _context.SpaceObject.Any(e => e.Id == id);
        }
    }
}
