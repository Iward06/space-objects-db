﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyWebsite.Models;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;  
using MyWebsite.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using System.Web;

namespace MyWebsite.Controllers
{
    public class LoginController : Controller
    {
        const string SessionUserName = "UserID";
        const string SessionUserID = "UserName";
        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User u)
        {
            string CS = "Server=(localdb)\\mssqllocaldb;Database=MyWebsiteContext-1;Trusted_Connection=True;MultipleActiveResultSets=true";
            if (ModelState.IsValid)
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Users WHERE UserName ='" + u.UserName + "' AND Password = '" + u.Password + "'", con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();



                    if (rdr.HasRows)
                    {
                        HttpContext.Session.SetString(SessionUserName, u.UserName);
                        HttpContext.Session.SetInt32(SessionUserID, u.UserID);
                        
                    }
                    else
                    {
                        HttpContext.Session.Clear();
                    }
                 


                    
                }
            }
            return View(u);
        }

        public IActionResult UserDashBoard()
        {
            if (SessionUserID != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}
/*public IActionResult Index(User u)
        {




           List<User> userList = new List<User>();


            string CS = "Server=(localdb)\\mssqllocaldb;Database=MyWebsiteContext-1;Trusted_Connection=True;MultipleActiveResultSets=true";

if (ModelState.IsValid)
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Users", con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        var user = new User();

                        user.UserID = Convert.ToInt32(rdr["UserId"]);
                        user.FullName = rdr["Fullname"].ToString();
                        user.UserName = rdr["Username"].ToString();
                        userList.Add(user);
                    }
                }
            }

*/

