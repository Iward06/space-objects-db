﻿CREATE TABLE [dbo].[SpaceObject] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Messier] INT            NOT NULL,
    [Common]  NVARCHAR (150) NULL,
    [Con]     NVARCHAR (150) NOT NULL,
    [Mag]     DECIMAL (18,3) NOT NULL, 
    [RA]      INT            NOT NULL,
    [DEC]     INT            NOT NULL,
    [Type]    NVARCHAR (150) NOT NULL,
    CONSTRAINT [PK_SpaceObject] PRIMARY KEY CLUSTERED ([Id] ASC)
);

