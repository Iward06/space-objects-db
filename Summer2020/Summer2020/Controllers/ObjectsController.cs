﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Summer2020.Data;
using Summer2020.Models;

namespace Summer2020.Controllers
{
    public class ObjectsController : Controller
    {
        private readonly Summer2020Context _context;

        public ObjectsController(Summer2020Context context)
        {
            _context = context;
        }

        // GET: Objects
        public async Task<IActionResult> Index()
        {
            return View(await _context.Objects.ToListAsync());
        }

        // GET: Objects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objects = await _context.Objects
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objects == null)
            {
                return NotFound();
            }

            return View(objects);
        }

        // GET: Objects/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Objects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Messier,Common,NGC,Con,Type,Mag,RA,DEC")] Objects objects)
        {
            if (ModelState.IsValid)
            {
                _context.Add(objects);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(objects);
        }

        // GET: Objects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objects = await _context.Objects.FindAsync(id);
            if (objects == null)
            {
                return NotFound();
            }
            return View(objects);
        }

        // POST: Objects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Messier,Common,NGC,Con,Type,Mag,RA,DEC")] Objects objects)
        {
            if (id != objects.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(objects);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ObjectsExists(objects.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(objects);
        }

        // GET: Objects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objects = await _context.Objects
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objects == null)
            {
                return NotFound();
            }

            return View(objects);
        }

        // POST: Objects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var objects = await _context.Objects.FindAsync(id);
            _context.Objects.Remove(objects);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ObjectsExists(int id)
        {
            return _context.Objects.Any(e => e.Id == id);
        }
    }
}
