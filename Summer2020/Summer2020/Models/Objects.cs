﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Summer2020.Models
{
    public class Objects
    {
        public int Id { get; set; }
        // [Display(Name = "Messier Value")]
        public string Messier { get; set; }
        //[Display(Name = "Common Name")]
        public string Common { get; set; }
        //[Display(Name = "New General Catalogue Value")]
        public int NGC { get; set; }
        //[Display(Name = "Constellation")]
        public string Con { get; set; }
        //[Display(Name = "Object Type")]
        public string Type { get; set; }
        // [Display(Name = "Magnitude")]
        public float Mag { get; set; }
        //[Display(Name = "Right Ascension")]
        public int RA { get; set; }
        //[Display(Name = "Declination")]
        public int DEC { get; set; }
    }
}
