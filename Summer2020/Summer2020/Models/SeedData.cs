﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Summer2020.Data;
using System;
using System.Linq;
using System.Reflection.PortableExecutable;

namespace Summer2020.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new Summer2020Context(
                serviceProvider.GetRequiredService<
                    DbContextOptions<Summer2020Context>>()))
            {
                if (context.Objects.Any())
                {
                    return;  
                }

                context.Objects.AddRange(
                    new Objects
                    {
                        Messier = 1,
                        Common = "s",
                        NGC = 3,
                        Con = "s",
                        Type = "x",
                        Mag = 9,
                        RA = 9,
                        DEC = 9,

                    },

                  new Objects
                  {
                      Messier = 1,
                      Common = "s",
                      NGC = 3,
                      Con = "s",
                      Type = "x",
                      Mag = 9,
                      RA = 9,
                      DEC = 9,

                  },

                  new Objects
                  {
                      Messier = 1,
                      Common = "s",
                      NGC = 3,
                      Con = "s",
                      Type = "x",
                      Mag = 9,
                      RA = 9,
                      DEC = 9,

                  },

                 new Objects
                 {
                     Messier = 1,
                     Common = "s",
                     NGC = 3,
                     Con = "s",
                     Type = "x",
                     Mag = 9,
                     RA = 9,
                     DEC = 9,

                 }
                );
                context.SaveChanges();
            }
        }
    }
}