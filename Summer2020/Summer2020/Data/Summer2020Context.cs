﻿using Microsoft.EntityFrameworkCore;
using Summer2020.Models;

namespace Summer2020.Data
{ 
    public class Summer2020Context : DbContext
    {
        public Summer2020Context (DbContextOptions<Summer2020Context> options)
            : base(options)
        {
        }
        public DbSet<Objects> Objects { get; set; }
    }
}