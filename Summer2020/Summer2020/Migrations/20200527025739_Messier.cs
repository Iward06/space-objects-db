﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Summer2020.Migrations
{
    public partial class Messier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Mesier",
                table: "Objects");

            migrationBuilder.AlterColumn<float>(
                name: "Mag",
                table: "Objects",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddColumn<int>(
                name: "Messier",
                table: "Objects",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Messier",
                table: "Objects");

            migrationBuilder.AlterColumn<int>(
                name: "Mag",
                table: "Objects",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(float));

            migrationBuilder.AddColumn<int>(
                name: "Mesier",
                table: "Objects",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }
    }
}
