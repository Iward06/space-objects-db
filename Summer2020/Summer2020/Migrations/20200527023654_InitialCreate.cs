﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Summer2020.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Objects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Mesier = table.Column<int>(nullable: false),
                    Common = table.Column<string>(nullable: true),
                    NGC = table.Column<int>(nullable: false),
                    Con = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Mag = table.Column<int>(nullable: false),
                    RA = table.Column<int>(nullable: false),
                    DEC = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Objects", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Objects");
        }
    }
}
